# Blurry Loading

This has the image starting blurred and with a loading counter.

This is part of a course by [Brad Traversy and Florin Pop](https://github.com/bradtraversy/50projects50days).

Image (c) Apple
